package org.bitbucket.infovillafoundation.wayyan.lesson1;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test1 {
    public static void main(String[] args) throws IOException {
        System.out.println("max(5, 10) -> " + max(5, 10));
        System.out.println("\nmaxOfThree(5, 10, 15) -> " + maxOfThree(5, 10, 15));
        System.out.println("\nifVowel('a') -> " + ifVowel('a'));
        System.out.println("\nreplaceAndTranslate(\"i love javascript\") -> " + replaceAndTranslate("i love javascript"));
        System.out.println("\nsum([10, 20, 30]) -> " + sum(10, 20, 30));
        System.out.println("\nmultiply([210, 20, 30]) -> " + multiply(10, 20, 30));
        System.out.println("\nreverse(\"some random text\") -> " + reverse("some random text"));
        System.out.println("\nfindLongestWord(\"find the longest word here\") -> " + findLongestWord("find the longest word here"));
        System.out.println("\nfilterLongWords(\"extremely boring long words\" , 5) -> " + filterLongWords("extremely boring long words", 5));
        System.out.println("\ncharFreq(\"abcdefghiijjkkll\") -> "+ charFreq("abcdefghiijjkkll"));
        System.out.println("\ntranslateChristmasCard(\"merry christmas and happy new year\") -> " + translateChristmasCard("merry christmas and happy new year"));
    }

    public static int max(int a, int b) {
        if (a > b)
            return a;
        else
            return b;
    }

    public static int maxOfThree(int a, int b, int c) {
        return max(max(a, b), c);
    }

    public static boolean ifVowel(char letter) {
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        for (char vowel : vowels)
            if (vowel == letter)
                return true;
        return false;
    }

    public static String replaceAndTranslate(String word) {
        String result = "";
        for (char character : word.toCharArray()) {
            if (character != 'a' && character != 'e' && character != 'i' && character != 'o' && character != 'u' && character != ' ') {
                result += Character.toString(character) + 'o' + Character.toString(character);
            } else {
                result += Character.toString(character);
            }
        }
        return result;
    }

    ;

    public static int sum(int... list) {
        int sum = 0;
        for (int item : list)
            sum += item;
        return sum;
    }

    public static int multiply(int... list) {
        int product = 1;
        for (int item : list)
            product *= item;
        return product;
    }

    public static String reverse(String text) {
        String rev = "";
        for (char letter : text.toCharArray())
            rev = letter + rev;
        return rev;
    }

    public static String translateChristmasCard(String text) throws IOException {
        ObjectMapper objectMappper = new ObjectMapper();
        Map<String, String> lexicon = objectMappper.readValue("{\"merry\": \"god\", \"christmas\": \"jul\", \"and\": \"och\", \"happy\": \"gott\", \"new\": \"nytt\", \"year\": \"år\"}", HashMap.class);
        String currentWord = "";
        String result = "";
        for (char letter : text.toCharArray()) {
            if (letter == ' ') {
                for (String key : lexicon.keySet()) {
                    if (key.equals(currentWord)) {
                        currentWord = lexicon.get(key);
                        result += currentWord + " ";
                        currentWord = "";
                    }
                }
            }
            else {
                currentWord += letter;
            }
        }
        result += lexicon.get(currentWord);
        return result;
    }


    public static int findLongestWord(String text) {
        int maxLength = 0;
        int wordLength = 0;
        for (char letter : text.toCharArray()) {
            if (letter == ' ') {
                if (wordLength > maxLength) {
                    maxLength = wordLength;

                }
                wordLength = 0;
            } else
                wordLength++;
        }
        return maxLength;
    }

    public static List<String> filterLongWords(String text, int i) {
        String currentWord = "";
        List<String> longWords = new ArrayList<String>();

        for (char character : text.toCharArray()) {
            if (character == ' ') {
                if (currentWord.length() > i) {
                    longWords.add(currentWord);
                }
                currentWord = "";

            } else {
                currentWord += character;
            }
        }
        return longWords;
    }

    public static Map<String, Integer> charFreq(String text) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Integer> frequency = objectMapper.readValue("{\"a\": 0, \"b\": 0, \"c\": 0, \"d\": 0, \"e\": 0, \"f\": 0, \"g\": 0, \"h\": 0, \"i\": 0, \"j\": 0, \"k\": 0, \"l\": 0, \"m\": 0, \"n\": 0, \"o\": 0, \"p\": 0, \"q\": 0, \"r\": 0, \"s\": 0, \"t\": 0, \"u\": 0, \"v\": 0, \"w\": 0, \"x\": 0, \"y\": 0, \"z\": 0}", HashMap.class);

        for (char letter : text.toCharArray()) {
            for (String key : frequency.keySet())
                if (key.equals(Character.toString(letter)))
                    frequency.put(key, frequency.get(key).intValue() + 1);
        }

        return frequency;
    }

}